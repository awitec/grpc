﻿using System;
using System.Threading.Tasks;
using GrpcTest.Contract;

namespace GrpcTest.Server.Host
{
    public static class Program
    {
        public static async Task Main(string[] args)
        {
            var server = new DataServiceServer("localhost", 8080, Security.ServerSide).Start();

            Console.WriteLine("Server is listening ... ");
            Console.ReadKey();

            await server.Stop();
        }
    }
}
