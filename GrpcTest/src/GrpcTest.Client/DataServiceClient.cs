﻿using System;
using System.IO;
using System.Threading.Tasks;
using Grpc.Core;
using GrpcTest.Contract;

namespace GrpcTest.Client
{
    public class DataServiceClient
    {
        private readonly Channel _channel;

        public DataServiceClient(string url, int port, Security security)
        {
            var credentials = ChannelCredentials.Insecure;

            if (security != Security.None)
            {
                var caCert = File.ReadAllText(@"./Certificates/ca.crt");
                var clientCert = File.ReadAllText(@"./Certificates/client.crt");
                var clientKey = File.ReadAllText(@"./Certificates/client.key");
                var serverCert = File.ReadAllText(@"./Certificates/server.crt");

                credentials = security == Security.Mutual
                    ? new SslCredentials(caCert, new KeyCertificatePair(clientCert, clientKey))
                    : new SslCredentials(serverCert, new KeyCertificatePair(clientCert, clientKey));
            }

            _channel = new Channel($"{url}:{port}", credentials);
        }

        public async Task<(string platform, string serverTime)> GetData()
        {
            var client = new DataService.DataServiceClient(_channel);

            var request = new DataRequest {Token = DateTime.Now.ToString("s")};

            var response = await client.GetDataAsync(request);

            return (response.Platform, response.ServerTime);
        }
    }
}
