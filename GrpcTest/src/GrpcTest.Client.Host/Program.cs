﻿using System;
using System.Threading.Tasks;
using GrpcTest.Contract;

namespace GrpcTest.Client.Host
{
    public static class Program
    {
        public static async Task Main(string[] args)
        {
            Console.WriteLine("Client ... ");

            var client = new DataServiceClient("localhost", 8080, Security.ServerSide);

            while (Console.ReadKey().Key != ConsoleKey.Q)
            {
                var (platform, serverTime) = await client.GetData();

                Console.WriteLine($"{serverTime} - {platform}");
            }
        }
    }
}
