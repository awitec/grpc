﻿using System;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;
using System.Collections.Generic;
using GrpcTest.Contract;
using Grpc.Core;

namespace GrpcTest.Server
{
    public class DataServiceServer: DataService.DataServiceBase
    {
        private readonly Grpc.Core.Server _server;

        public DataServiceServer(string url, int port, Security security)
        {
            var credentials = ServerCredentials.Insecure;

            if (security != Security.None)
            {
                var caCert = File.ReadAllText(@"./Certificates/ca.crt");
                var serverCert = File.ReadAllText(@"./Certificates/server.crt");
                var serverKey = File.ReadAllText(@"./Certificates/server.key");
                var keyPair = new KeyCertificatePair(serverCert, serverKey);

                credentials = security == Security.Mutual
                    ? new SslServerCredentials(new List<KeyCertificatePair> {keyPair}, caCert, SslClientCertificateRequestType.RequestAndRequireAndVerify)
                    : new SslServerCredentials(new List<KeyCertificatePair> {keyPair});
            }

            _server = new Grpc.Core.Server
            {
                Services = { DataService.BindService(this) },
                Ports = { new ServerPort(url, port, credentials) }
            };
        }

        public DataServiceServer Start()
        {
            _server.Start();

            return this;
        }

        public async Task Stop()
        {
            await _server.ShutdownAsync();
        }

        public override Task<DataResponse> GetData(DataRequest request, ServerCallContext context)
        {
            Console.WriteLine("Request: " + request.Token);

            return Task.FromResult(new DataResponse
            {
                Platform = RuntimeInformation.OSDescription,
                ServerTime = DateTime.Now.ToString("s")
            });
        }
    }
}